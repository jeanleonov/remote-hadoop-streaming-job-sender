import os
import traceback
from datetime import datetime
import cherrypy
from subprocess import call
from multiprocessing import Process
import logging

STREAMING_JAR_PATH = "/home/hadoop/hadoop-install/share/hadoop/tools/lib/" \
                     "hadoop-streaming-2.7.1.jar"
LAST_LINE_LABEL = "$THE END$"


class HadoopProxy(object):
    def __init__(self):
        cherrypy.config.update({
            'server.socket_host': "192.168.0.3",
            'server.socket_port': 50993
        })

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def start_hadoop_job(self):
        try:
            req = cherrypy.request.json
            user_name = req["user_name"]
            dfs_input_name = req["dfs_input_name"]
            mapper_script = req["mapper_script"]
            reducer_script = req["reducer_script"]
            dfs_output_name = req["dfs_output_name"]
            input_format = req["input_format"]
            output_format = req["output_format"]
            partitioner = req["partitioner"]
            combiner = req["combiner"]
            verbose = req["verbose"]
            num_reduce_tasks = req["num_reduce_tasks"]

            dir_name = "../clients/{user}".format(user=user_name)
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
            mapper_name = "{dir}/mapper".format(dir=dir_name)
            with open(mapper_name, mode="w") as mapper:
                mapper.write(mapper_script)
            reducer_name = "{dir}/reducer".format(dir=dir_name)
            with open(reducer_name, mode="w") as reducer:
                reducer.write(reducer_script)

            job_output_name = str(datetime.utcnow())
            output_name = "{dir}/{job_name}"\
                .format(dir=dir_name, job_name=job_output_name)
            open(output_name, mode="w").close()

            # Required args first
            args = [
                "hadoop", "jar", STREAMING_JAR_PATH,
                "-mapper", mapper_name,
                "-reducer", reducer_name,
                "-input", dfs_input_name,
                "-output", "students/{user}/{path}".format(
                    user=user_name, path=dfs_output_name),
                "-file", mapper_name,
                "-file", reducer_name
            ]
            # Optional args
            if input_format:
                args.append("-inputformat")
                args.append(input_format)
            if output_format:
                args.append("-outputformat")
                args.append(output_format)
            if partitioner:
                args.append("-partitioner")
                args.append(partitioner)
            if combiner:
                args.append("-combiner")
                args.append(combiner)
            if verbose:
                args.append("-verbose")
                args.append(verbose)
            if num_reduce_tasks:
                args.append("-numReduceTasks")
                args.append(num_reduce_tasks)

            def job_manager():
                with open(output_name, mode="w") as output_:
                    call(args, stdout=output_, stderr=output_)
                    output_.write("\n"+LAST_LINE_LABEL)

            Process(target=job_manager).start()
            return {
                "job_name": job_output_name,
                "cursor": 0
            }
        except Exception as err:
            details = traceback.format_exc()
            logging.error(details)
            return {
                "error": "{err}\n{stack}".format(err=err, stack=details)
            }

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def get_output(self):
        try:
            req = cherrypy.request.json
            user_name = req["user_name"]
            job_name = req["job_name"]
            cursor = req["cursor"]

            output_name = "../clients/{user}/{job_name}"\
                .format(user=user_name, job_name=job_name)
            with open(output_name) as output:
                hadoop_jar_output = output.read()
            output_lines = [
                unicode(line, errors="ignore") if isinstance(line, str) else line
                for line in hadoop_jar_output.splitlines()
            ]
            if output_lines and output_lines[-1] == LAST_LINE_LABEL:
                is_finished = True
                try:
                    os.remove(output_name)
                except IOError:
                    details = traceback.format_exc()
                    logging.error(details)
            else:
                is_finished = False
            new_lines = output_lines[cursor:-1]
            return {
                "output": u"\n".join(new_lines),
                "cursor": None if is_finished else (cursor + len(new_lines))
            }
        except Exception as err:
            details = traceback.format_exc()
            logging.error(details)
            return {"error": "{err}\n{stack}".format(err=err, stack=details)}


if __name__ == '__main__':
    cherrypy.quickstart(HadoopProxy(), '/')
