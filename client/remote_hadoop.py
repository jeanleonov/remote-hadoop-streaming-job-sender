import json
import requests
import argparse
import sys
import traceback

import time

if __name__ == '__main__':
    try:
        # Configure arguments parser
        description = 'Sends streaming jobs to remote hadoop'
        parser = argparse.ArgumentParser(description=description)
        parser.add_argument("-user", type=str)
        parser.add_argument("-mapper", type=str)
        parser.add_argument("-reducer", type=str)
        parser.add_argument("-input", type=str)
        parser.add_argument("-output", type=str)
        parser.add_argument("-server_address", type=str,
                            default="http://104.155.91.17:50993")
        parser.add_argument("-inputformat", type=str, default=None)
        parser.add_argument("-outputformat", type=str, default=None)
        parser.add_argument("-partitioner", type=str, default=None)
        parser.add_argument("-combiner", type=str, default=None)
        parser.add_argument("-verbose", type=str, default=None)
        parser.add_argument("-numReduceTasks", type=str, default=None)

        args = parser.parse_args()
        with open(args.mapper) as mapper_file:
            mapper = mapper_file.read()
        with open(args.reducer) as reducer_file:
            reducer = reducer_file.read()

        user_name = args.user
        if not user_name:
            raise Exception("User name (-user YOUR_NAME) is required!")
        output_name = args.output
        response = requests.post(
            "{address}/start_hadoop_job".format(address=args.server_address),
            json.dumps({
                "user_name": user_name,
                "dfs_input_name": args.input,
                "mapper_script": mapper,
                "reducer_script": reducer,
                "dfs_output_name": output_name,
                "input_format": args.inputformat,
                "output_format": args.outputformat,
                "partitioner": args.partitioner,
                "combiner": args.combiner,
                "verbose": args.verbose,
                "num_reduce_tasks": args.numReduceTasks
            }),
            headers={"Content-type": "application/json"}
        ).json()

        if "error" in response:
            sys.stderr.write(response["error"])
            exit(1)

        job_name = response["job_name"]
        cursor = response["cursor"]
        while cursor is not None:
            response = requests.post(
                "{address}/get_output".format(address=args.server_address),
                json.dumps({
                    "user_name": user_name,
                    "job_name": job_name,
                    "cursor": cursor
                }),
                headers={"Content-type": "application/json"}
            ).json()

            if "error" in response:
                sys.stderr.write(response["error"])
                exit(1)

            output = response["output"]
            cursor = response["cursor"]
            if output:
                print output
            time.sleep(0.5)

        print \
            "If task has been finished successfully, " \
            "you should be able to brows the result here:\n" \
            "https://console.developers.google.com/storage/browser/" \
            "teamworkretail-hadoop-training/students/{user}/{output_dir}" \
            .format(user=user_name, output_dir=output_name)

    except Exception as err:
        print err
        traceback.print_exc()
        sys.exit(1)
