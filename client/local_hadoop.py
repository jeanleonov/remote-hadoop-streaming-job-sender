import subprocess
import argparse
import os
import re
import sys

description = 'Sends streaming jobs not to remote hadoop'
parser = argparse.ArgumentParser(description=description)
parser.add_argument("-input", type=str, default="input.txt")
parser.add_argument("-output", type=str, default="output.txt")
parser.add_argument("-mapper", type=str, default="mapper.py")
parser.add_argument("-reducer", type=str, default="reducer.py")

def normalizate(path):
    return re.sub(r"\\|/", path, os.sep)

args = parser.parse_args()
p = "python "
dir_ = os.getcwd()
dir_ += "/"

mapper_comand = normalizate(dir_ + args.mapper)
reducer_comand = normalizate(dir_ + args.reducer)

input_ = open(normalizate(dir_ + args.input), "rb")
output_ = open(normalizate(dir_ + args.output), "wb")

if os.name != "nt":
    mapper = subprocess.Popen(p + mapper_comand, shell = True, stdin = input_, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    mapper.wait()
    sort_ = subprocess.Popen(p + os.path.dirname(os.path.realpath(__file__)) + "/sort.py", shell = True, stdin = mapper.stdout, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    sort_.wait()
    reducer = subprocess.Popen(p + reducer_comand, shell = True, stdin = sort_.stdout, stdout=output_, stderr=subprocess.PIPE)
    reducer.wait()
else:
    mapper = subprocess.Popen([sys.executable, mapper_comand], stdin = input_, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    mapper.wait()
    sort_ = subprocess.Popen([sys.executable, os.path.dirname(os.path.realpath(__file__)) +  "/sort.py"], stdin = mapper.stdout, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    sort_.wait()
    reducer = subprocess.Popen([sys.executable, reducer_comand], stdin = sort_.stdout, stdout=output_, stderr=subprocess.STDOUT)
    reducer.wait()


mapper.stdout.close()
sort_.stdout.close()
input_.close()
output_.close()
