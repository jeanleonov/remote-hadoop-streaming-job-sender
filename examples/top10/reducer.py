#!/usr/bin/python
import sys

# Example input (ordered by key)
# FALSE 1
# FALSE 1
# TRUE 1
# TRUE 1
# UNKNOWN 1
# UNKNOWN 1

# keys come grouped together
# so we need to keep track of state a little bit
# thus when the key changes (word), we need to reset
# our counter, and write out the count we've accumulated

last_word = None
word_count = 0

words_dict = {}


for line in sys.stdin:
    line = line.strip()
    word, count = line.split("\t")
    count = int(count)
    # if this is the first iteration
    if not last_word:
        last_word = word

    # if they're the same, log it
    if word == last_word:
        word_count += count
    else:
        # state change (previous line was k=x, this line is k=y)
        out = "\t".join((last_word, str(word_count)))
        words_dict[last_word] = word_count
        last_word = word
        word_count = 1

a = sorted(words_dict.items(), key=lambda (k, v): v, reverse=True)
for i in range(10):
    print a[i]

