#!/usr/bin/python
import sys
import math

fraq = 1
den_res = 0
s = ""
counter = 0
denominator = 0
mult = 0
ranger = 0

for line in sys.stdin:
    line = line.strip()
    a = line.split("\t")
    denominator, mult = int(a[1]), int(a[2])
    fraq = fraq * mult
    den_res = den_res * mult + denominator
    ranger = int(a[0])

def accuracy(maxN):
    return int(maxN * math.log(maxN))

for i in xrange( accuracy(ranger) ):
    if i != 0:
        s += str(den_res / fraq)
    else:
        s += '2'
    den_res %= fraq
    den_res *= 10
    if i == 0:
        s += '.'

print s
