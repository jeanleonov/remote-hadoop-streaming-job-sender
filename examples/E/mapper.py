#!/usr/bin/python
import sys

l, r = 0, 0
denominator = 0
remainder = 1

for line in sys.stdin.readlines():
    line = line.strip()
    denominator = 0
    remainder = 1
    unpacked = line.split(" ")

    l = int(unpacked[0])
    r = int(unpacked[1])

    for i in range(l, r + 1):
        denominator = denominator * i + 1
        remainder *= i
    print "{:010d}\t{}\t{}".format(r, denominator, remainder)
